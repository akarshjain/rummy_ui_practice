﻿using System.Collections.Generic;
using UnityEngine;

public class GroupCards : MonoBehaviour {


    public static int CARD_DIFF = 60;

    public static int GROUP_DIFF = 50;
    public static int CARD_WIDTH = 180;


    protected List<List<GameObject>> lstGroups;

    protected List<GameObject> lstGroup1;
    protected List<GameObject> lstGroup2;
    protected List<GameObject> lstGroup3;
    protected List<GameObject> lstGroup4;
    protected List<GameObject> lstGroup5;
    protected List<GameObject> lstGroup6;

    protected Dictionary<int, Vector2Int> mapCardsGrooup;
    protected Dictionary<int, int> mapGroupStartLocalPos;
    protected Dictionary<int, int> mapGroupIndexGNumber;

    public GameObject mainParent;

    public GameObject cardHolder;
    public GameObject card1;
    public GameObject card2;
    public GameObject card3;
    public GameObject card4;
    public GameObject card5;
    public GameObject card6;
    public GameObject card7;
    public GameObject card8;
    public GameObject card9;
    public GameObject card10;
    public GameObject card11;
    public GameObject card12;
    public GameObject card13;
    public GameObject card14;


    // Use this for initialization
    void Start () {
        mapCardsGrooup = new Dictionary<int, Vector2Int>();
        mapGroupStartLocalPos = new Dictionary<int, int>();
        mapGroupIndexGNumber = new Dictionary<int, int>();
        lstGroups = new List<List<GameObject>>();

        lstGroup1 = new List<GameObject>
        {
            card1,
            card2,
            card3
        };


        lstGroup2 = new List<GameObject>
        {
            card4,
            card5

        };

        lstGroup3 = new List<GameObject>
        {
            card8,
            card9,
            card10
        };

        lstGroup4 = new List<GameObject>
        {
            card11,
            card12
        };

        lstGroup5 = new List<GameObject>
        {
            card13,
            card14
        };

        lstGroup6 = new List<GameObject>
        {
            card6,
            card7
        };

        lstGroups.Add(lstGroup1);
        lstGroups.Add(lstGroup2);
        lstGroups.Add(lstGroup3);
        lstGroups.Add(lstGroup4);
        lstGroups.Add(lstGroup5);
        lstGroups.Add(lstGroup6);

        SetCardGroups(true);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void SortCardGroups() { 

        for(int i =0; i< lstGroups.Count; i++)
        {
            if(lstGroups[i].Count == 1 && i != lstGroups.Count - 1)
            {
                if(lstGroups[lstGroups.Count - 1].Count == 1)
                {
                    GameObject obj = lstGroups[i][0];
                    lstGroups[i].RemoveAt(0);
                    lstGroups[lstGroups.Count - 1].Insert(1, obj);
                    obj.transform.SetAsLastSibling();
                }
                else
                {
                    List<GameObject> gameObjects = lstGroups[i];
                    lstGroups.Remove(gameObjects);
                    lstGroups.Insert(lstGroups.Count, gameObjects);
                }
            }
        }
    }


    void SetCardGroups(bool bResetGrpPos)
    {
        int z = 0;
        int x = 0;
        for(int i =0; i<lstGroups.Count; i++)
        {
            List<GameObject> lstCards = lstGroups[i];
            if(lstCards.Count == 0 && bResetGrpPos)
            {
                Debug.Log(i);
                lstGroups.Remove(lstCards);
                i--;
                continue;
            }
            //mapGroupIndexGNumber[i] = 
            mapGroupStartLocalPos[i] = x;
            for(int j =0; j < lstCards.Count; j++)
            {
                if(lstCards[j] != null && lstCards[j].GetComponent<CardDrag>() != null)
                {
                    int nCardNumber = lstCards[j].GetComponent<CardDrag>().nCardValue;
                    mapCardsGrooup[nCardNumber] = new Vector2Int(i, j);
                    z++;
                }
                GameObject obj = lstCards[j];
                if (obj != null)
                {
                    obj.gameObject.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
                    obj.gameObject.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
                    obj.gameObject.GetComponent<RectTransform>().pivot = new Vector2(0f, 0f);
                    obj.gameObject.transform.localPosition = new Vector2(x, 0);
                }

                if(j != lstCards.Count -1)
                    x += CARD_DIFF;
            }

            if(lstGroups[i].Count != 0)
                x += CARD_WIDTH;
            if (lstGroups[i].Count != 0 && i != lstGroups.Count - 1)
                x += GROUP_DIFF;
        }

        float nTW = mainParent.gameObject.GetComponent<RectTransform>().sizeDelta.x;

        float nLeft = (nTW - x) / 2;
        cardHolder.gameObject.transform.localPosition = new Vector2(nLeft,0);
        cardHolder.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(x, cardHolder.gameObject.GetComponent<RectTransform>().sizeDelta.y);
    }

    public Vector2Int GetCardGroup(int nCard)
    {
        return mapCardsGrooup[nCard];
    }

    public int GetCardNumber(Vector2Int nCardPos)
    {
        int nIndex = 0;
        Debug.Log("Card final pos is " + nCardPos);
        if (lstGroups[nCardPos.x].Count == 0)
        {
            nIndex = -1;
        }
        else
        {

            if (lstGroups[nCardPos.x].Count > nCardPos.y + 1)
            {
                nIndex = lstGroups[nCardPos.x][nCardPos.y + 1].gameObject.transform.GetSiblingIndex();
            }
            else
            {
                nIndex = lstGroups[nCardPos.x][nCardPos.y - 1].gameObject.transform.GetSiblingIndex();
                nIndex++;
            }
        }

        return nIndex;
    }

    public void SetCardFinalPos(Vector2Int newPos, GameObject card)
    {
        lstGroups[newPos.x].RemoveAt(newPos.y);
        lstGroups[newPos.x].Insert(newPos.y, card);
        SetCardGroups(true);
        SortCardGroups();
        SetCardGroups(true);

        if (lstGroups.Count < 6)
        {
            lstGroups.Insert(lstGroups.Count, new List<GameObject>());
        }
    }

    public Vector2Int SetNewCardPos(float nDisplacement, bool bDragBegin , int nCard, Vector2Int prePos, float nPosition)
    {
        if(bDragBegin)
        {
            Vector2Int origPos = GetCardGroup(nCard);
            lstGroups[origPos.x].RemoveAt(origPos.y);
            lstGroups[origPos.x].Insert(origPos.y, null);
            return origPos;
        }

        int nNewGropuPos = 0;
        int nNewCardPos = 0;
        Vector2Int nNewPos = prePos;

        if(nDisplacement > 0)
        {
            int nOrigGrp = prePos.x;

            for(int i = lstGroups.Count - 1; i >= nOrigGrp; i--)
            {
                if(mapGroupStartLocalPos[i] - GROUP_DIFF / 2 < nPosition)
                {
                    nNewGropuPos = i;
                    //Debug.Log("new group is " + i);

                    float nDiffernceInGrp = nPosition - mapGroupStartLocalPos[i];
                    nNewCardPos = Mathf.RoundToInt(nDiffernceInGrp) / CARD_DIFF;

                    if(nNewCardPos < 0)
                    {
                        nNewCardPos = 0;
                    }

                    int nFac = 0;
                    if (nNewGropuPos == nNewPos.x)
                    {
                        nFac = 1;
                    }

                    if (nNewCardPos > lstGroups[i].Count - nFac)
                    {
                        nNewCardPos = lstGroups[i].Count - nFac;
                    }

                    //Debug.Log("new Card location is " + nNewCardPos);
                    break;
                }
            }
        }
        else
        {
            int nOrigGrp = prePos.x;
            for (int i = nOrigGrp; i >= 0; i--)
            {
                //Debug.Log("displacement -ve " + (mapGroupStartLocalPos[i] - GROUP_DIFF / 2) + " position is " + nPosition);
                if (mapGroupStartLocalPos[i] - GROUP_DIFF / 2 < nPosition)
                {
                    nNewGropuPos = i;
                    Debug.Log("new group is " + i);

                    float nDiffernceInGrp = nPosition - mapGroupStartLocalPos[i];
                    nNewCardPos = Mathf.RoundToInt(nDiffernceInGrp) / CARD_DIFF;

                    Debug.Log(" new card pos " + nNewCardPos + " diff if"  + Mathf.RoundToInt(nDiffernceInGrp));

                    if (nNewCardPos < 0)
                    {
                        nNewCardPos = 0;
                    }

                    int nFac = 0;
                    if (nNewGropuPos == nNewPos.x)
                    {
                        nFac = 1;
                    }

                    if (nNewCardPos > lstGroups[i].Count - nFac)
                    {
                        nNewCardPos = lstGroups[i].Count - nFac;
                    }

                    Debug.Log("new Card location is " + nNewCardPos);
                    break;
                }
            }
        }

        if (nNewGropuPos != nNewPos.x || nNewCardPos != nNewPos.y)
        {
            Debug.Log("deleting from positon " + nNewPos);
            lstGroups[nNewPos.x].RemoveAt(nNewPos.y);
            nNewPos = new Vector2Int(nNewGropuPos, nNewCardPos);
            Debug.Log("inserting into positon " + nNewPos);
            lstGroups[nNewGropuPos].Insert(nNewCardPos, null);

            SetCardGroups(false);

        }

        return nNewPos;
    }
}
