﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardDrag : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler {

    public int nCardValue = 0;
    Vector2Int mCardInitPosition;
    GroupCards mGroupCards;
    Vector2 mInitPosition;
    Vector2 mInitPointerPosition;
    Vector2Int mCardPreviousPosition;
    Vector2Int mCardCurentPosition;

    public void OnBeginDrag(PointerEventData eventData)
    {
        mCardInitPosition = mGroupCards.GetCardGroup(nCardValue);
        mCardPreviousPosition = mCardInitPosition;
        Debug.Log("card position is " + mCardInitPosition.x + mCardInitPosition.y);
        mInitPosition = transform.localPosition;
        mInitPointerPosition = eventData.position;
        transform.SetAsLastSibling();
        mGroupCards.SetNewCardPos(0, true, nCardValue, mCardInitPosition, mInitPosition.x);

    }

    public void OnDrag(PointerEventData eventData)
    {
        float nInitPos = transform.localPosition.x;
        transform.position = Input.mousePosition;
        transform.position = new Vector2(transform.position.x, transform.position.y - mInitPointerPosition.y);

        //Debug.Log("displacent " + (transform.localPosition.x - nInitPos));

        mCardPreviousPosition = mGroupCards.SetNewCardPos((transform.localPosition.x - nInitPos), false, nCardValue, 
            mCardPreviousPosition, transform.localPosition.x);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        mGroupCards.SetCardFinalPos(mCardPreviousPosition, this.gameObject);
        //transform.localPosition = mInitPosition;
        mCardCurentPosition = mGroupCards.GetCardGroup(nCardValue);
        int nIndex = mGroupCards.GetCardNumber(mCardCurentPosition);
        if (nIndex != -1)
        {
            transform.SetSiblingIndex(nIndex);
        }
        else
        {
            transform.SetAsLastSibling();
        }

    }

    void Start()
    {

        mGroupCards = FindObjectOfType<GroupCards>();
    }
}
